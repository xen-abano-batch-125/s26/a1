db.course_bookings.insertMany(
	[
		{"courseID": "C001", "studentID":"S004", "isCompleted":true},
		{"courseID": "C002", "studentID":"S001", "isCompleted":false},
		{"courseID": "C001", "studentID":"S003", "isCompleted":true},
		{"courseID": "C003", "studentID":"S002", "isCompleted":false},
		{"courseID": "C001", "studentID":"S002", "isCompleted":true},
		{"courseID": "C004", "studentID":"S003", "isCompleted":false},
		{"courseID": "C002", "studentID":"S004", "isCompleted":true},
		{"courseID": "C003", "studentID":"S007", "isCompleted":false},
		{"courseID": "C001", "studentID":"S005", "isCompleted":true},
		{"courseID": "C004", "studentID":"S008", "isCompleted":false},
		{"courseID": "C001", "studentID":"S013", "isCompleted":true}
	]
);

//count the completed courses of student s013.
	db.course_bookings.aggregate(
		[
			{$match:{"studentID":"S013"}},
			{$group: {_id: null,count: {$sum:1}}},
		]
	);


//show the students who are not yet completed in their course, without showing the course IDs.
	db.course_bookings.aggregate(
		[
			{$match:{"isCompleted":false}},
			{$project:{"courseID":0}}
		]
	);

//sort the courseID in descending order while studentID in ascending order
	db.course_bookings.aggregate(
		[
			{$sort:{"courseID":-1, "studentID":1}}
		]
	);